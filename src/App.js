import React from 'react';
import './App.css';
import { observer } from 'mobx-react';
import CryptoPage from './crypto/CryptoPage';
import './styles/index.scss';
// import './styles/chartiq.scss';
// import './styles/stx-chart.css';
// import './styles/cryptoiq.scss';
// import { HomePage } from './pages/home/Home';

const App = observer(() => {
  return (
    <div className="App">
      <CryptoPage />
    </div>
  );
});

export default App;
