import { IAnyModelType, Instance, types } from 'mobx-state-tree';
import { DataModel, PageModel, PageType } from '../internal';

export const RootModel: IAnyModelType = types
  .model('Root', {
    api: types.frozen({}),
    pages: types.optional(types.map(types.union(PageModel)), {}),
    data: DataModel,
  })
  .actions((self) => {
    return {
      afterCreate() {
        Object.defineProperty(window, 'root', {
          get() {
            return self;
          },
        });
      },
      addPage(page: PageType) {
        self.pages.put(page);
      },
      resetApi() {
        // @ts-ignore
        self.api.init(baseUrl);
      },
    };
  });
export type RootType = Instance<typeof RootModel>;
