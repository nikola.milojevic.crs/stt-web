import { getRoot, IAnyModelType, Instance, types } from 'mobx-state-tree';

import { RootType } from '../internal';

export const DataModel: IAnyModelType = types
  .model('DataModel', {
    test: '123',
  })
  .views((self) => {
    return {
      get root(): RootType {
        return getRoot(self);
      },
    };
  })
  .actions((self) => {
    return {};
  });

export type DataType = Instance<typeof DataModel>;
