import React from 'react';
import { observer } from 'mobx-react';

const CryptoPage = observer(() => {
  return (
    <div className="crypto-page">
      <h1>Crypto</h1>
    </div>
  );
});

export default CryptoPage;
