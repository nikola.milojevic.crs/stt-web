import { RootModel } from './internal';
import { createContext, useContext } from 'react';

export const rootInstance = RootModel.create({
  api: {},
  data: {},
});

const RootStoreContext = createContext(rootInstance);

export const Provider = RootStoreContext.Provider;
export function useMst() {
  const store = useContext(RootStoreContext);
  if (store === null) {
    throw new Error('Store cannot be null, please add a context provider');
  }
  return store;
}

export const ProviderWrapper = ({ children }) => {
  return <Provider value={rootInstance}>{children}</Provider>;
};
