//********* FunctionModel.ts must come before component models *********//

export * from './models/FunctionModel';

//********* Component models must come before utility and major models *********//

export * from './models/ComponentModel';

//********* Container models must come before PageRoutes and Router *********//

export * from './models/PageModel';

//********* PageRoutes, ViewModel and Router are the last before the RootModel  *********//

export * from './models/ViewModel';

//********* Data models must come before PageRoutes and Router *********//

export * from './models/DataModel';

//********* RootModel MUST be the last model to be loaded *********//

export * from './models/RootModel';

//********* The store is instantiated last in order for the tests to run properly  *********//
export * from './store';
